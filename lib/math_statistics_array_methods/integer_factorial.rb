module MathStatisticsArrayMethods

	##
	# Will add the factorial method to the integers.
	##
	module IntegerFactorial

		##
		# Classic factorial.
		##
		def factorial
			f = 1
			for i in 1..self do
				f *= i
			end
			return f
		end
	end
end

##
# Add the methods to the Integer class
##
class Integer
	prepend MathStatisticsArrayMethods::IntegerFactorial
end
