# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'math_statistics_array_methods/version'

Gem::Specification.new do |spec|
  spec.name          = "math_statistics_array_methods"
  spec.version       = MathStatisticsArrayMethods::VERSION
  spec.authors       = ["Leo Benkel"]
  spec.email         = ["leo.benkel@gmail.com"]

  spec.summary       = "Will provide a set of methods for the arrays to do statistics math."
  spec.description   = "Will provide a set of methods for the arrays to do statistics math."
  spec.homepage      = "https://gitlab.com/wonay/math_statistics_array_methods"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.11"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "byebug"
  spec.add_development_dependency "minitest", "~> 5.0"
end
