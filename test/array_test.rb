require 'test_helper'

##
# Test array methods
##
class ArrayMethodsTest < Minitest::Test

	def test_sample_average
		array = Array.new(9) {|n| n}
		assert_equal 9 / 2 , array.sample_average
	end

	def test_median
		array = Array.new(9) {|n| n}
		assert_equal array.sample_average, array.median

		array = Array.new(10) {|n| n}
		assert_equal array.sample_average, array.median
	end

	def test_variance
		array = Array.new(10) {|n| n}
		assert_equal 8.25, array.variance
	end

	def test_sample_standard_deviation
		array = Array.new(10) { |i| i }
		assert_equal 2.87, array.sample_standard_deviation.round(2)
	end

	def test_frequencies
		array = Array.new(10) {|i| i}
		assert_equal array.inject({}) {|hash, n| hash[n] = 1; hash } , array.frequencies
	end

	def test_mode
		array = Array.new(10){ |i| i }
		array[0] = 5
		assert_equal [5], array.mode
	end

	def test_probability
		array = Array.new(10){ |i| i }
		assert_equal 0.1, array.probability_of(1)

		array[0] = 5
		assert_equal 0.1, array.probability_of(1)
		assert_equal 0.2, array.probability_of(5)
	end

	def test_permutation_quantity
		array = Array.new(10){ |i| i }
		assert_equal array.length.factorial, array.permutation_quantity
	end

	def test_combination_quantity
		array = Array.new(10){ |i| i }
		assert_equal 1, array.combination_quantity
	end

	def test_permutation_with_repetition
		array = Array.new(10){ |i| i }
		assert_equal array.length**array.length, array.permuation_repetition_quantity
	end

	def test_range
		array = Array.new(10){ |i| i }
		assert_equal 9, array.range
	end

end
